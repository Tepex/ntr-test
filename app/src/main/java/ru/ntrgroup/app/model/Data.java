package ru.ntrgroup.app.model;

/* должен быть Parcellable */
public class Data
{
	@Override
	public String toString()
	{
		return "[id = "+id+", name="+name+", type="+type+", date_opened="+date_opened+"]";
	}
	
	public int getId()
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getType()
	{
		return type;
	}
	
	/*
	public Address getAddress()
	{
		return address;
	}
	*/
	
	private int id;
	private String name;
	private String type;
	private String date_opened;
	private String date_closed;
	//private Address address;
}