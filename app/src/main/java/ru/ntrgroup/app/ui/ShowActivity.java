package ru.ntrgroup.app.ui;

import android.annotation.SuppressLint;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;

import android.util.Log;

import android.view.View;

import android.widget.TextView;
import android.widget.Toast;

import ru.ntrgroup.app.BuildConfig;
import ru.ntrgroup.app.model.Data;
import ru.ntrgroup.app.R;
import ru.ntrgroup.app.Utils;

import static ru.ntrgroup.app.Utils.TAG;

public class ShowActivity extends BaseActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "Start show activity");
		if(!onCreate(savedInstanceState, R.layout.show_layout)) return;
		
		TextView idText = (TextView)findViewById(R.id.l_id);
		TextView nameText = (TextView)findViewById(R.id.l_name);
		TextView typeText = (TextView)findViewById(R.id.l_type);
		
		Bundle extras = getIntent().getExtras();
		if(extras != null)
		{
			idText.setText(getString(R.string.l_id)+extras.getInt("id"));
			nameText.setText(getString(R.string.l_name)+extras.getString("name"));
			typeText.setText(getString(R.string.l_type)+extras.getString("type"));
		}
	}
}