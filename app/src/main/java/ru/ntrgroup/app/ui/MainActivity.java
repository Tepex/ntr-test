package ru.ntrgroup.app.ui;

import android.annotation.SuppressLint;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;

import android.util.Log;

import android.view.View;

import android.widget.Toast;

import com.octo.android.robospice.request.listener.RequestListener;

import com.octo.android.robospice.exception.NetworkException;
import com.octo.android.robospice.exception.RequestCancelledException;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.persistence.DurationInMillis;

import ru.ntrgroup.app.BuildConfig;
import ru.ntrgroup.app.model.Data;
import ru.ntrgroup.app.network.DataRequest;
import ru.ntrgroup.app.R;
import ru.ntrgroup.app.Utils;

import static ru.ntrgroup.app.Utils.TAG;

public class MainActivity extends BaseActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "Start main activity");
		if(!onCreate(savedInstanceState, R.layout.main_layout)) return;
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
	}
	
	@Override
	protected void onRestart()
	{
		super.onRestart();
		if(BuildConfig.DEBUG) Log.d(TAG, "onRestart MainActivity");
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();
	}
	
	public void getData(View view)
	{
		showProgressDialog(getString(R.string.app_name), getString(R.string.data_load));
		DataRequest request = new DataRequest();
		spiceManager.execute(request, DataRequest.CACHE_KEY, DurationInMillis.ALWAYS_EXPIRED, new DataRequestListener());
	}
	
	private void showData(Data data)
	{
		progressDialog.dismiss();
		Log.d(TAG, "data "+data);
		Intent intent = new Intent(this, ShowActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		/* по хорошему надо обернуть в Parcelable, но нет времени */
		intent.putExtra("id", data.getId());
		intent.putExtra("name", data.getName());
		intent.putExtra("type", data.getType());
		startActivity(intent);
		finish();
	}
	
	public final class DataRequestListener implements RequestListener<Data>
	{
		@Override
		public void onRequestFailure(SpiceException se)
		{
			if(BuildConfig.DEBUG) Log.e(TAG, "Rest error: ", se);
			cancelProgressDialog();
			if(se instanceof RequestCancelledException) return;
			Toast.makeText(MainActivity.this, "Network error", Toast.LENGTH_LONG).show();
		}
		
		@Override
		public void onRequestSuccess(final Data data)
		{
			cancelProgressDialog();
			showData(data);
        }
	}
}
