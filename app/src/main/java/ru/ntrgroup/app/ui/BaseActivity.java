package ru.ntrgroup.app.ui;

import android.app.ProgressDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.widget.Toast;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.SpiceManager;

import ru.ntrgroup.app.BuildConfig;
import ru.ntrgroup.app.NTRSpiceService;
import ru.ntrgroup.app.R;
import ru.ntrgroup.app.Utils;

import static ru.ntrgroup.app.Utils.TAG;

abstract class BaseActivity extends AppCompatActivity
{
	protected boolean onCreate(Bundle bundle, @LayoutRes int layoutId)
	{
		super.onCreate(bundle);
		if(BuildConfig.DEBUG) Log.d(TAG, "Base activity");
		setContentView(layoutId);
	
		return true;
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		spiceManager.start(this);
	}
	
	@Override
	protected void onStop()
	{
		spiceManager.shouldStop();
		super.onStop();
	}
	
	protected void showProgressDialog(String title, String message)
	{
		progressDialog = ProgressDialog.show(this, title, message, true, true, new DialogInterface.OnCancelListener()
		{
			@Override
			public void onCancel(DialogInterface dialog)
			{
				spiceManager.cancelAllRequests();
			}
		});
	}
	
	protected void cancelProgressDialog()
	{
		if(progressDialog != null) progressDialog.cancel();
	}
	
	protected ProgressDialog progressDialog;
	protected SpiceManager spiceManager = new SpiceManager(NTRSpiceService.class);
}
