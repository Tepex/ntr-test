package ru.ntrgroup.app;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

import ru.ntrgroup.app.network.Rest;
import ru.ntrgroup.app.network.RestApi;

import retrofit.RestAdapter;

public class NTRSpiceService extends RetrofitGsonSpiceService
{
	@Override
	public void onCreate()
	{
		super.onCreate();
		addRetrofitInterface(RestApi.class);
	}
	
	@Override
	protected RestAdapter.Builder createRestAdapterBuilder()
	{
		return Rest.getRestBuilder(Rest.MAIN);
	}
	
	@Override
	protected String getServerUrl()
	{
		return Rest.MAIN;
	}
}