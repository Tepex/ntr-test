package ru.ntrgroup.app.network;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import ru.ntrgroup.app.model.Data;

public class DataRequest extends RetrofitSpiceRequest<Data, RestApi>
{
	public DataRequest()
	{
		super(Data.class, RestApi.class);
	}
	
	@Override
	public Data loadDataFromNetwork() throws Exception
	{
		return getService().getData();
	}
	
	public static final String CACHE_KEY = "data";
}