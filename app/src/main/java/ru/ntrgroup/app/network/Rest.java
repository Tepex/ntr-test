package ru.ntrgroup.app.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;

import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

import ru.ntrgroup.app.BuildConfig;
import ru.ntrgroup.app.R;
import ru.ntrgroup.app.TestApplication;

public class Rest
{
	public static synchronized RestApi getApi(String baseUrl)
	{
		if(api == null)
		{
			RestAdapter adapter = getRestBuilder(baseUrl).build();
			api = adapter.create(RestApi.class);
		}
		return api;
	}
	
	public static Gson getGson()
	{
		return gson;
	}
	
	public static synchronized RestAdapter.Builder getRestBuilder(String baseUrl)
	{
		if(restBuilder == null)
		{
			restBuilder = new RestAdapter.Builder();
			restBuilder.setEndpoint(baseUrl).setConverter(new GsonConverter(gson));
			if(BuildConfig.DEBUG) restBuilder.setLogLevel(RestAdapter.LogLevel.FULL);
			else restBuilder.setLogLevel(RestAdapter.LogLevel.NONE);
		}
		return restBuilder;
	}
	
	private static RestApi api;
	private static RestAdapter.Builder restBuilder;
	
	//public static final String DF = "dd.MM.yyyy HH:mm:ss";
	public static final String DF = "yyyy-MM-dd HH:mm:ss";
	
	private static Gson gson = new GsonBuilder()
		.excludeFieldsWithModifiers(Modifier.STATIC, Modifier.TRANSIENT)
		.setDateFormat(DF)
		.create();	
	
	public static final String MAIN = TestApplication.getContext().getString(R.string.src_url);
}