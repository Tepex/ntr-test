package ru.ntrgroup.app.network;

import retrofit.http.GET;
import ru.ntrgroup.app.model.Data;

public interface RestApi
{
	@GET("/test.json")
	public Data getData() throws RestException;
}
