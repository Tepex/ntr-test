package ru.ntrgroup.app;

import android.app.Application;
import android.content.Context;

public class TestApplication extends Application
{
	@Override
	public void onCreate()
	{
		INSTANCE = this;
		super.onCreate();
	}
	
	public static Context getContext()
	{
		return INSTANCE;
	}
	
	public static TestApplication getInstance()
	{
		return INSTANCE;
	}
	
	private static TestApplication INSTANCE;
}